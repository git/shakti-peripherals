package ifc_sync;

  import Clocks::*;
  import GetPut::*;

  (*always_ready,always_enabled*)
  interface Ifc_sync#(type a);
    (*always_ready,always_enabled*)
    interface Put#(a) put;
    (*always_ready,always_enabled*)
    interface Get#(a) get;
  endinterface
  module mksyncconnection#(Clock putclock, Reset putreset,
                           Clock getclock, Reset getreset)(Ifc_sync#(a))
    provisos(Bits#(a, a__));
    CrossingReg#(a) null_wire<- mkNullCrossingReg(getclock,?,
                                        clocked_by putclock,
                                        reset_by putreset);
 //   ReadOnly#(Bit#(a)) null_wire <- mkNullCrossingWire(getclock, 
//                                  from_put, clocked_by getclock,
//                                  reset_by getreset);
    interface put = interface Put
      method Action put(a in);
        null_wire<= in;
      endmethod
    endinterface;
    interface get = interface Get
      method ActionValue#(a) get();
        return null_wire.crossed;
      endmethod
    endinterface;
  endmodule

endpackage

