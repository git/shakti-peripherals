/* 
Copyright (c) 2013, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
 with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Neel Gala
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package rgbttl_dummy;
  `define RGBTTL_WIDTH 18
  `include "instance_defines.bsv"
  import GetPut::*;
  import BUtils ::*;
  import AXI4_Types::*;

  interface Ifc_rgbttl_dummy;
	    interface AXI4_Master_IFC#(`PADDR, `DATA, `USERSPACE) master;
	    interface AXI4_Slave_IFC#(`PADDR, `DATA, `USERSPACE) slave;
      (* always_ready *) interface Get#(Bit#(1)) de;
      (* always_ready *) interface Get#(Bit#(1)) ck;
      (* always_ready *) interface Get#(Bit#(1)) vs;
      (* always_ready *) interface Get#(Bit#(1)) hs;
      (* always_ready *) interface Get#(Bit#(`RGBTTL_WIDTH)) data_out;
  endinterface

  (*synthesize*)
  module mkrgbttl_dummy(Ifc_rgbttl_dummy);
	  	AXI4_Slave_Xactor_IFC#(`PADDR,`DATA, `USERSPACE)
                            s_xactor<-mkAXI4_Slave_Xactor();
	  	AXI4_Master_Xactor_IFC#(`PADDR,`DATA, `USERSPACE)
                            m_xactor<-mkAXI4_Master_Xactor();

      Reg#(Bit#(1)) rg_de <- mkReg(0);
      Reg#(Bit#(1)) rg_ck <- mkReg(0);
      Reg#(Bit#(1)) rg_vs <- mkReg(0);
      Reg#(Bit#(1)) rg_hs <- mkReg(0);
      Reg#(Bit#(`RGBTTL_WIDTH)) rg_data <- mkReg(0);

      interface de = interface Get
        method ActionValue#(Bit#(1)) get;
          return rg_de;
        endmethod
      endinterface;

      interface ck = interface Get
        method ActionValue#(Bit#(1)) get;
          return rg_ck;
        endmethod
      endinterface;

      interface vs = interface Get
        method ActionValue#(Bit#(1)) get;
          return rg_vs;
        endmethod
      endinterface;

      interface hs = interface Get
        method ActionValue#(Bit#(1)) get;
          return rg_hs;
        endmethod
      endinterface;

      interface data_out = interface Get
        method ActionValue#(Bit#(`RGBTTL_WIDTH)) get;
          return rg_data;
        endmethod
      endinterface;

      interface slave=s_xactor.axi_side;
      interface master=m_xactor.axi_side;
  endmodule
endpackage
